// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";


contract FundMe {
    uint256 public minDonationUSD = 50;

    /* The donor can send the money.
     * payable make this function interact with the value input in the Deploy tab.
     */
    function fund () public payable {
        // Set a minimal amount of 1Eth.
        require(msg.value > 1e18, "Minimum donation is 1Eth.");
    }

    // https://docs.chain.link/docs/data-feeds/price-feeds/

    function getPrice () public {
        // We're interacting with a contract from outside.
        // So we need ABI and Address.
        // ABI:
        // - Imported with the Aggregator interface
        // Address:
        // - available here https://docs.chain.link/docs/data-feeds/price-feeds/addresses/
        // - Look for Goerli Network
        // - ETH/USD Price -> ETH / USD	0xD4a33860578De61DBAbDc8BFdb98FD742fA7028e
        AggregatorV3Interface(0xD4a33860578De61DBAbDc8BFdb98FD742fA7028e);
    }

    function getConversionRate () public {

    }

    // The owner of the contract can withdraw the money.
    // function withdraw () {

    // }


}