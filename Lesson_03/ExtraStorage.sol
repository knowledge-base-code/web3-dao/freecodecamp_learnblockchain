// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;

import "./SimpleStorage.sol";

contract ExtraStorage is SimpleStorage {
    function store (uint256 _favoriteNumber) public override {
        // favoriteNumber is global in SimpleStorage.
        favoriteNumber = _favoriteNumber + 5;
    }
}