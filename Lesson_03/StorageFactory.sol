// SPDX-License-Identifier: MIT

pragma solidity ^0.8.7;
import "./SimpleStorage.sol";

contract StorageFactory {
    SimpleStorage[] public simpleStores;

    function createSimpleStorageContract() public {
        simpleStores.push(new SimpleStorage());
    }

    /**
        To interact with a contract, you'll need:
            1. The address of the contrat
            2. The ABI, prepackaged in the compilation of the contract
                You can access the ABI by importing the sol file
     */
    function factoryStore(uint256 _index, uint256 _value) public {
        // ABI - like API but Binary. Application Binary Interface.
        simpleStores[_index].store(_value);
    }

    function factoryStoreGet(uint256 _index) public view returns(uint256) {
        return simpleStores[_index].retrieve();
    }
}